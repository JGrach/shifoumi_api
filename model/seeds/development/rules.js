exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('rules')
    .del()
    .then(function() {
      // Inserts seed entries
      return knex('rules').insert([
        { title: 'truc', roundTime: 8000, roundNumber: 3 },
      ])
    })
}
