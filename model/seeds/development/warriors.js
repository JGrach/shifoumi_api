exports.seed = function(knex, Promise) {
  return knex('warriors')
    .del()
    .then(function() {
      return knex('warriors').insert([
        {
          weapon: 'rock',
          isWinner: false,
          order: 1,
          users_id: 1,
        },
        {
          weapon: 'paper',
          isWinner: true,
          order: 1,
          users_id: 2,
        },
        {
          weapon: 'cissors',
          isWinner: false,
          order: 2,
          users_id: 1,
        },
        {
          weapon: 'rock',
          isWinner: true,
          order: 2,
          users_id: 2,
        },
      ])
    })
}
