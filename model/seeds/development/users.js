exports.seed = function(knex, Promise) {
  return knex('users')
    .del()
    .then(function() {
      return knex('users').insert([
        {
          //id: 1,
          login: 'admin',
          mailAddress: 'admin@admin.admin',
          password:
            '$2a$10$SV04SPwA6GbQVFLoX3F20OudqZ6sk4dw75qpmoUpoDfKvLdjW6WjW', //admin123
          isAdmin: true,
        },
        {
          //id: 2,
          login: 'f699634d-2857-40f8-a498-0f267',
          mailAddress: 'GameMaster@shifoumi.co',
          password:
            '$2a$10$F.R7ni4jof.0rDdU02xFEuEzwV6ptKjgWG.qwkaSCCjiR6sWp/gIy', // 2maxYa;gsya2.+k6
          isAdmin: true,
        },
        {
          //id: 3,
          login: 'test',
          mailAddress: 'test@test.test',
          password:
            '$2a$10$SV04SPwA6GbQVFLoX3F20OudqZ6sk4dw75qpmoUpoDfKvLdjW6WjW', //admin123
          isAdmin: false,
        },
      ])
    })
}
