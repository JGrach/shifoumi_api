const knex = require('./knex')

function rule() {
  return knex('rules')
}

function add(ruleNew, cb) {
  rule()
    .insert(ruleNew)
    .returning('id')
    .into('rules')
    .asCallback(cb)
}

function getBy(constraints, cb) {
  rule()
    .where(constraints)
    .asCallback(cb)
}

function getAll(cb) {
  rule().asCallback(cb)
}

function update(constraints, newDatas, cb) {
  rule()
    .where(constraints)
    .update(newDatas)
    .asCallback(cb)
}

function delRule(id, cb) {
  rule()
    .where(id)
    .del()
    .asCallback(cb)
}

module.exports = {
  add,
  getBy,
  getAll,
  update,
  delRule,
}
