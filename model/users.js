const knex = require('./knex')

function user() {
  return knex('users')
}

function add(userNew, cb) {
  user()
    .insert(userNew)
    .returning('id')
    .into('users')
    .asCallback(cb)
}

function getBy(constraints, cb, showAll = false, showAdmin = true) {
  const keyReadable = showAll ? null : ['id', 'login']
  if (!showAdmin && !showAll) constraints.isAdmin = false
  user()
    .where(constraints)
    .select(keyReadable)
    .asCallback(cb)
}

function getAll(cb, showAll = false, showAdmin = true) {
  const keyReadable = showAll ? null : ['id', 'login']
  if (!showAdmin && !showAll) {
    user()
      .select(keyReadable)
      .where({ isAdmin: false })
      .asCallback(cb)
  } else {
    user()
      .select(keyReadable)
      .asCallback(cb)
  }
}

function update(constraints, newDatas, cb) {
  user()
    .where(constraints)
    .update(newDatas)
    .asCallback(cb)
}

function delUser(id, cb) {
  user()
    .where(id)
    .del()
    .asCallback(cb)
}

module.exports = {
  add,
  getBy,
  getAll,
  update,
  delUser,
}
