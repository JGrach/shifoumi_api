const knex = require('./knex'),
  { formate, roundCombination } = require('./aggregation')

function round() {
  return knex('warriors_games as round')
}

const schematic = {
  game: [0, 2],
  warrior: [2, 8],
  user: [8],
  aggregator: 'warrior',
}

function getRoundResultAll(cb, toMachine = false) {
  round()
  .select('round.games_id', 'warriors.weapon', 'warriors.isWinner', 'warriors.order')
    .join('warriors', 'round.warriors_id', 'warriors.id')
    .join('users', 'warriors.users_id', 'users.id')
    .join('games', 'round.games_id', 'games.id')
    .asCallback((err, res) => {
      if(err) return cb(err)
      return cb(null, res)
    })
}

function getRoundResultBy(constraints, cb, toMachine = false) {
  round()
    .select('round.games_id', 'warriors.weapon', 'warriors.isWinner', 'warriors.order')
    .join('warriors', 'round.warriors_id', 'warriors.id')
    .join('users', 'warriors.users_id', 'users.id')
    .join('games', 'round.games_id', 'games.id')
    .where(constraints)
    .asCallback((err, res) => {
      if(err) return cb(err)
      return cb(null, res)
    })
}

module.exports = {
  getRoundResultBy,
  getRoundResultAll,
}
