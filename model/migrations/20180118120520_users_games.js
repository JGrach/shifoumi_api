// delete
exports.up = (knex, Promise) => {
  // CREATE TABLE `users_games` (
  // `users_id` int UNSIGNED NOT NULL,
  // `games_id` int UNSIGNED NOT NULL,
  // `isWinner` boolean NOTNULL DEFAULT false,
  // FOREIGN KEY (users_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  // FOREIGN KEY (games_id) REFERENCES games(id) ON DELETE CASCADE ON UPDATE CASCADE;
  //)
  return knex.schema.createTable('users_games', table => {
    table
      .integer('users_id')
      .unsigned()
      .notNullable()
    table
      .foreign('users_id')
      .references('users.id')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
    table
      .integer('games_id')
      .unsigned()
      .notNullable()
    table
      .foreign('games_id')
      .references('games.id')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
    table
      .boolean('isWinner')
      .notNullable()
      .defaultTo('false')
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('users_games')
}
