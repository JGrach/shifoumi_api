exports.up = (knex, Promise) => {
  // CREATE TABLE `games` (
  // `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  // `created at` datetime,
  // `created at` datetime DEFAULT CURRENT_TIMESTAMP,
  // `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  // `rules_id` int UNSIGNED NOT NULL,
  // PRIMARY KEY (id),
  // FOREIGN KEY (rules_id) REFERENCES rules(id) ON DELETE CASCADE ON UPDATE CASCADE;
  //)
  return knex.schema.createTable('games', table => {
    table.increments()
    table.timestamps(false, true)
    table
      .integer('rules_id')
      .unsigned()
      .notNullable()
    table
      .foreign('rules_id')
      .references('rules.id')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('games')
}
