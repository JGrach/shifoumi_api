exports.up = (knex, Promise) => {
  // CREATE TABLE `warriors_games` (
  // `warriors_id` int UNSIGNED NOT NULL,
  // `games_id` int UNSIGNED NOT NULL,
  // FOREIGN KEY (warriors_id) REFERENCES warriors(id) ON DELETE CASCADE ON UPDATE CASCADE,
  // FOREIGN KEY (games_id) REFERENCES games(id) ON DELETE CASCADE ON UPDATE CASCADE;
  return knex.schema.createTable('warriors_games', table => {
    table
      .integer('warriors_id')
      .unsigned()
      .notNullable()
    table
      .foreign('warriors_id')
      .references('warriors.id')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
    table
      .integer('games_id')
      .unsigned()
      .notNullable()
    table
      .foreign('games_id')
      .references('games.id')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('warriors_games')
}
