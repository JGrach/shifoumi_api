exports.up = (knex, Promise) => {
  // CREATE TABLE `rules` (
  // `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  // `created at` datetime DEFAULT CURRENT_TIMESTAMP,
  // `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  // `title` varchar(255) NOT NULL,
  // `gameTime` int NOT NULL,
  // `roundTime` int NOT NULL,
  // `roundNumber` int NOT NULL,
  // PRIMARY KEY (id);
  //)
  return knex.schema.createTable('rules', table => {
    table.increments()
    table.timestamps(false, true)
    table
      .string('title')
      .notNullable()
      .unique()
    // table.integer('gameTime').notNullable()
    table.integer('roundTime').notNullable()
    table.integer('roundNumber').notNullable()
    table.text('comments')
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('rules')
}
