exports.up = (knex, Promise) => {
  // CREATE TABLE `warriors` (
  // `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  // `created at` datetime DEFAULT CURRENT_TIMESTAMP,
  // `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  // `weapon` varchar(255),
  // `isWinner` boolean NOT NULL DEFAULT false,
  // `order` int NOT NULL,
  // `users_id` int UNSIGNED NOT NULL,
  // PRIMARY KEY (id),
  // FOREIGN KEY (users_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE;
  //)
  return knex.schema.createTable('warriors', table => {
    table.increments()
    table.timestamps(false, true)
    table.string('weapon')
    table
      .boolean('isWinner')
      .notNullable()
      .defaultTo('false')
    table.integer('order').notNullable()
    table
      .integer('users_id')
      .unsigned()
      .notNullable()
    table
      .foreign('users_id')
      .references('users.id')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('warriors')
}
