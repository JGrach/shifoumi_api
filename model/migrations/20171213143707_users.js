exports.up = (knex, Promise) => {
  // CREATE TABLE `users` (
  // `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  // `created at` datetime DEFAULT CURRENT_TIMESTAMP,
  // `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  // `login` varchar(255) NOT NULL,
  // `mailAddress` varchar(255) NOT NULL,
  // `password` varchar(255) NOT NULL,
  // `isAdmin` boolean NOTNULL DEFAULT false,
  // PRIMARY KEY (id);
  //)
  return knex.schema.createTable('users', table => {
    table.increments()
    table.timestamps(false, true)
    table.string('login').notNullable()
    table
      .string('mailAddress')
      .notNullable()
      .unique()
    table.string('password').notNullable()
    table
      .boolean('isAdmin')
      .notNullable()
      .defaultTo('false')
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('users')
}
