const knex = require('./knex'),
  { formate } = require('./aggregation')

function game() {
  return knex('games')
}

const schematic = {
  game: [0, 4],
  users: [4, 7],
  warriors: [7],
  aggregator: 'game',
}

function add(gameNew, cb) {
  game()
    .insert(gameNew)
    .returning('id')
    .into('games')
    .asCallback(cb)
}

function addRelation(table, relationNew, cb) {
  knex(table + '_games')
    .insert(relationNew)
    .asCallback(cb)
}

function getBy(constraints, cb, toMachine = false) {
  let subConstraints = { users_ids: [], warriors_ids: [] }
  if(constraints.users_ids){
    subConstraints.users_ids = [...constraints.users_ids]
    delete constraints.users_ids
  }
  if(constraints.warriors_ids){
    subConstraints.warriors_ids = [...constraints.warriors_ids]
    delete constraints.warriors_ids
  }

  let query = game()
    .select(
      'games.*',
      'users.login',
      'users.id as users_id',
      'users_games.isWinner',
      'warriors.id as warriors_id',
      'warriors.created_at as warriors_created_at',
      'warriors.updated_at as warriors_updated_at',
      'warriors.weapon as warriors_weapon',
      'warriors.isWinner as warriors_isWinner',
      'warriors.order as warriors_order',
      'warriors.users_id as warriors_users_id'
    )
    .join('users_games', 'games.id', 'users_games.games_id')
    .join('users', 'users_games.users_id', 'users.id')
    .leftJoin('warriors_games', 'games.id', 'warriors_games.games_id')
    .leftJoin('warriors', 'warriors_games.warriors_id', 'warriors.id')
    .where(constraints)
    
  if(constraints.users_ids){
    query = query.whereIn('users_id', subConstraints.users_ids)
  }
  if(constraints.warriors_ids){
    query = query.whereIn('warriors_id', subConstraints.warriors_ids)
  }

  query.asCallback(formate(cb, schematic, toMachine))
}

function getAll(cb, toMachine = false) {
  game()
    .select(
      'games.*',
      'users.login',
      'users.id as users_id',
      'users_games.isWinner',
      'warriors.id as warriors_id',
      'warriors.created_at as warriors_created_at',
      'warriors.updated_at as warriors_updated_at',
      'warriors.weapon as warriors_weapon',
      'warriors.isWinner as warriors_isWinner',
      'warriors.order as warriors_order',
      'warriors.users_id as warriors_users_id'
    )
    .join('users_games', 'games.id', 'users_games.games_id')
    .join('users', 'users_games.users_id', 'users.id')
    .leftJoin('warriors_games', 'games.id', 'warriors_games.games_id')
    .leftJoin('warriors', 'warriors_games.warriors_id', 'warriors.id')
    .asCallback(formate(cb, schematic, toMachine))
}

function update(constraints, newDatas, cb) {
  game()
    .where(constraints)
    .update(newDatas)
    .asCallback(cb)
}

function delgame(id, cb) {
  game()
    .where(id)
    .del()
    .asCallback(cb)
}

module.exports = {
  add,
  addRelation,
  getBy,
  getAll,
  update,
  delgame,
}
