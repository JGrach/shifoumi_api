/* Search if aggregator already exist in the constructing response array
for each aggregator we have an entry in the array so findAggregator
return the index of the current aggregator or -1 if it doesn't exist */
function findAggregator(query, constructed, aggregator, aggIsNotTheMain) {
  const id = aggIsNotTheMain
    ? aggregator + (aggregator.endsWith('s') ? '_id' : 's_id')
    : 'id'
  const index = constructed.findIndex(
    object => object[aggregator][id] == query[id] // ex: {user: {users_id: 1}}
  )
  return index
}

/* Create a new entry in the constructing response array and attach to it the aggregator object
return the index of this new entry */
function createAggregator(
  constructed,
  aggregatorDefinition,
  aggregator,
  index
) {
  // work with a copy but constructed stay a reference
  const agg = {}
  agg[aggregator] = aggregatorDefinition
  constructed.push(agg)
  return constructed.length - 1
}

/* Split the current object of the initial SQL response
return the object splited */
function sliceRequest(query, low, high) {
  if (!high) high = undefined
  // slice create a copy
  const part = Object.keys(query)
    .slice(low, high)
    .reduce((newObject, key) => {
      newObject[key] = query[key]
      return newObject
    }, {})
  const isEmpty = Object.keys(part).reduce(
    (isEmpty, key) => (isEmpty ? part[key] === null : false),
    true
  )
  return isEmpty ? null : part
}

/* Push in an object of the current entry of the constructing array response
a key and an object given by the sliceRequest return nothing */
function pushSubQuery(query, location, subQuery, name) {
  if (!subQuery) return
  const index = location[name].findIndex(
    object => object['id'] == query[name + '_id']
  )
  if (index == -1) {
    location[name].push(deletePrefix(name, subQuery))
  }
  return
}

/* Replace the current object by a copy where keys are reformated to delete the
prefix return the new object */
function deletePrefix(prefix, object) {
  if (!object) return object
  const newObject = {}
  Object.keys(object).map(key => {
    newObject[key.replace(prefix + '_', '')] = object[key]
  })
  return newObject
}

/* return an object with the splited objects given by sliceRequest */
function formatedCanvas(query, separators) {
  return Object.keys(separators).reduce((object, key) => {
    if (key == 'aggregator') return object
    if (!separators[key][1]) {
      object[key] = sliceRequest(query, separators[key][0])
      return object
    }
    object[key] = sliceRequest(query, separators[key][0], separators[key][1])
    return object
  }, {})
}

function formate(cb, separators, machine) {
  return (err, res) => {
    if (err) return cb(err)
    if (res.length < 1) return cb(null, res)
    const resFormated = res.reduce((resFormating, query) => {
      const canvas = formatedCanvas(query, separators)
      if (machine) {
        resFormating.push(canvas)
        return resFormating
      }
      let index = findAggregator(query, resFormating, separators.aggregator)
      if (index == -1) {
        index = createAggregator(
          resFormating,
          canvas[separators.aggregator],
          separators.aggregator,
          index
        )
      }
      Object.keys(separators).map(separator => {
        if (separator == separators.aggregator || separator == 'aggregator')
          return
        if (!resFormating[index][separator]) resFormating[index][separator] = []
        pushSubQuery(query, resFormating[index], canvas[separator], separator)
      })
      return resFormating
    }, [])
    cb(null, resFormated)
  }
}

function roundCombination(queryFormated){
  return queryFormated.reduce((warriorsByRound, warrior) => {
    console.log('\n'+JSON.stringify(warriorsByRound))
    const roundIndex = warriorsByRound.findIndex( round => round.length === 1 && 
      round[0].game.games_id === warrior.game.games_id && 
      round[0].warrior.order === warrior.warrior.order
    )
    if(roundIndex !== -1) warriorsByRound[roundIndex].push(warrior)
    else warriorsByRound.push([warrior])
    return warriorsByRound
  }, [])
}

module.exports = {
  formate,
  roundCombination
}
