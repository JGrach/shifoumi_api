const knex = require('./knex'),
  { formate } = require('./aggregation')

function warrior() {
  return knex('warriors')
}

const schematic = {
  warrior: [0, 6],
  user: [6],
  aggregator: 'user',
}

function add(warriorNew, cb) {
  warrior()
    .insert(warriorNew)
    .returning('id')
    .into('warriors')
    .asCallback(cb)
}

function getBy(constraints, cb, toMachine = false) {
  constraints = Object.keys(constraints).reduce((acc, key) => {
    acc['warriors.' + key] = constraints[key]
    return acc
  }, {})
  warrior()
    .select('warriors.*', 'users.login')
    .join('users', 'warriors.users_id', 'users.id')
    .where(constraints)
    .asCallback(formate(cb, schematic, toMachine))
}

function getAll(cb, toMachine = false) {
  warrior()
    .select('warriors.*', 'users.login')
    .join('users', 'warriors.users_id', 'users.id')
    .asCallback(formate(cb, schematic, toMachine))
}

function update(constraints, newDatas, cb) {
  warrior()
    .where(constraints)
    .update(newDatas)
    .asCallback(cb)
}

function delWarrior(id, cb) {
  warrior()
    .where(id)
    .del()
    .asCallback(cb)
}

module.exports = {
  add,
  getBy,
  getAll,
  update,
  delWarrior,
}
