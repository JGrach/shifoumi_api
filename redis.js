const env = process.env.NODE_ENV || 'development',
  { redis } = require('./config.json')[env]
const r = require('redis'),
  client = r.createClient({
    host: redis.host,
    port: redis.port,
  })

module.exports = client
