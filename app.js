const express = require('express'),
  app = express(),
  cors = require('cors')

const { logger } = require('./handlers/middlewares'),
  public = require('./routes/public'),
  admin = require('./routes/admin'),
  users = require('./routes/users'),
  connect = require('./routes/connected'),
  { connected } = require('./handlers/middlewares'),
  env = process.env.NODE_ENV || 'development',
  {
    api,
    api: { version },
  } = require('./config.json')[env],
  v = '/v' + version

if(env === 'development') require('dotenv').config() &&  require('dotenv').config({ path: 'bdd.env'})

app.use(logger)
app.use(cors())
app.use(express.json())
app.use(connected)

app.use(v + '/admin', admin)
app.use(v + '/users?', users)
app.use(v, public)
app.use(v, connect)

// app.use('*', 404)

app.listen(api.port, function(err) {
  console.log("API is running, waiting for request\n")
})
