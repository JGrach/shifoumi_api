const express = require('express'),
  router = express.Router()
const usersCtrl = require('../controllers/users'),
  authCtrl = require('../controllers/authentication')

router.get('/users', usersCtrl.getUsers)
router.post('/user', authCtrl.addUser)
router.post('/login', authCtrl.login)

module.exports = router
