const express = require('express')
const router = express.Router()
const usersCtrl = require('../controllers/users'),
  { isConnected, isMe } = require('../handlers/middlewares')

router
  .route('/:id')
  .all(isConnected)
  .get(usersCtrl.getUser)
  .put(isMe, usersCtrl.updateUser)
  .delete(isMe, usersCtrl.deleteUser)

module.exports = router
