const express = require('express'),
  router = express.Router()
const { isConnected } = require('../handlers/middlewares'),
  rulesCtrl = require('../controllers/rules'),
  warriorsCtrl = require('../controllers/warriors'),
  gamesCtrl = require('../controllers/games'),
  measureCtrl = require('../controllers/measures')

router.use(isConnected)

router.get('/rules?', rulesCtrl.getRules)
router.get('/rules?By', rulesCtrl.getRuleBy) // queried

router.get('/warriors?', warriorsCtrl.getWarriors)
router.get('/warriors?By', warriorsCtrl.getWarriorBy) // queried
router.get('/measures?/:name', measureCtrl.getMeasures)

router.get('/games?', gamesCtrl.getGames)
router.get('/games?By', gamesCtrl.getGameBy) // queried

module.exports = router
