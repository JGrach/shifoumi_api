const express = require('express'),
  router = express.Router()
const { isAdmin } = require('../handlers/middlewares'),
  usersCtrl = require('../controllers/users'),
  warriorsCtrl = require('../controllers/warriors'),
  gamesCtrl = require('../controllers/games'),
  rulesCtrl = require('../controllers/rules')

router.use(isAdmin)

router.get('/rules?', rulesCtrl.getRules)
router.post('/rules?', rulesCtrl.addRule)
router.get('/rules?By', rulesCtrl.getRuleBy) // queried
router.put('/rules?/:id', rulesCtrl.updateRule)
router.delete('/rules?/:id', rulesCtrl.deleteRule)

router.post('/warriors?', warriorsCtrl.addWarrior)
router.put('/warriors?/:id', warriorsCtrl.updateWarrior)
router.delete('/warriors?/:id', warriorsCtrl.deleteWarrior)

router.post('/games?', gamesCtrl.addGame)
router.put('/games?/:id', gamesCtrl.updateGame)
router.delete('/games?/:id', gamesCtrl.deleteGame)

module.exports = router
