const env = process.env.NODE_ENV || 'development',
  { database } = require('./config.json')[env]

if(env === 'development') require('dotenv').config({ path: 'bdd.env' })

console.log(database)

module.exports = {
  test: {
    client: 'pg',
    connection: {
      host: database.host,
      port: database.port,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB
    },
    migrations: {
      directory: __dirname + '/model/migrations',
    },
    seeds: {
      directory: __dirname + '/model/seeds/test',
    },
  },
  development: {
    client: 'pg',
    connection: {
      host: database.host,
      port: database.port,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB
    },
    migrations: {
      directory: __dirname + '/model/migrations',
    },
    seeds: {
      directory: __dirname + '/model/seeds/development',
    },
  },
  docker: {
    client: 'pg',
    connection: {
      host: database.host,
      port: database.port,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB
    },
    migrations: {
      directory: __dirname + '/model/migrations',
    },
    seeds: {
      directory: __dirname + '/model/seeds/development',
    },
  },
  production: {
    client: 'pg',
    connection: {
      host: database.host,
      port: database.port,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB
    },
    migrations: {
      directory: __dirname + '/model/migrations',
    },
    seeds: {
      directory: __dirname + '/model/seeds/production',
    },
  },
}
