const Joi = require('joi')

const userSchemaAll = Joi.object().keys({
  id: Joi.number().integer(),
  created_at: Joi.date().iso(),
  updated_at: Joi.date().iso(),
  login: Joi.string()
    .min(3)
    .max(30),
  mailAddress: Joi.string()
    .min(3)
    .max(30),
  password: Joi.string()
    .min(6)
    .max(40),
  isAdmin: Joi.boolean(),
})
const userSchema = userSchemaAll.forbiddenKeys('id', 'created_at', 'updated_at')

const ruleSchemaAll = Joi.object().keys({
  id: Joi.number().integer(),
  created_at: Joi.date().iso(),
  updated_at: Joi.date().iso(),
  title: Joi.string()
    .min(3)
    .max(30),
  // gameTime: Joi.number().integer(),
  roundTime: Joi.number().integer(),
  roundNumber: Joi.number().integer(),
  comment: Joi.string().max(100),
})
const ruleSchema = ruleSchemaAll.forbiddenKeys('id', 'created_at', 'updated_at')

const gameSchemaAll = Joi.object().keys({
  id: Joi.number().integer(),
  created_at: Joi.date().iso(),
  updated_at: Joi.date().iso(),
  rules_id: Joi.number().integer(),
  users_objects: Joi.array().items(Joi.object().keys({
    id: Joi.number().integer().required(),
    isWinner: Joi.boolean().required(),
  })),
  warriors_ids: Joi.array().items(Joi.number().integer()),
  users_ids: Joi.array().items(Joi.number().integer()),
})
const gameSchema = gameSchemaAll.forbiddenKeys('id', 'created_at', 'updated_at')

const warriorSchemaAll = Joi.object().keys({
  id: Joi.number().integer(),
  created_at: Joi.date().iso(),
  updated_at: Joi.date().iso(),
  weapon: Joi.string()
    .valid('rock', 'paper', 'scissors')
    .allow(null),
  isWinner: Joi.boolean(),
  order: Joi.number().integer(),
  users_id: Joi.number().integer(),
})
const warriorSchema = warriorSchemaAll.forbiddenKeys(
  'id',
  'created_at',
  'updated_at'
)

const measureSchemaAll = Joi.object().keys({
  name: Joi.string().valid('quantity').required(),
  pov: Joi.string().valid('mine')
})

const arrayOf = (schema, max) => {
  if (!!max) {
    return Joi.array()
      .items(schema)
      .max(max)
  }
  return Joi.array().items(schema)
}
const id = Joi.object()
  .keys({ id: Joi.number().integer() })
  .requiredKeys('id')

module.exports = {
  userSchema,
  userSchemaAll,
  ruleSchema,
  ruleSchemaAll,
  gameSchema,
  gameSchemaAll,
  warriorSchema,
  warriorSchemaAll,
  measureSchemaAll,
  arrayOf,
  id,
}
