const jwt = require('jsonwebtoken'),
  { every } = require('async'),
  uuid = require('uuid/v4')
const redis = require('../redis'),
  { logger } = require('./helpers')

// AUTHENTICATION VERIFICATION
const extractBearerToken = bearerToken => {
  if (typeof bearerToken !== 'string') {
    return false
  }
  const matches = bearerToken.match(/(bearer)\s+(\S+)/i)
  return matches && matches[2]
}

const tokenExtraction = (token, cb) => {
  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) return cb(err)
    cb(null, decoded)
  })
}

//CREATION
function newAuth(auth, cb) {
  const redisId = uuid()
  every(
    Object.keys(auth).map(key => {
      const pair = {}
      pair[key] = auth[key]
      return pair
    }),
    (pair, callback) => {
      const key = Object.keys(pair)
      if (key.length > 1)
        return callback(
          new Error(
            '500: Error during user object decomposition for Redis newAuth()'
          )
        )
      redis.HSET(redisId, key[0], auth[key[0]], callback)
    },
    err => {
      if (err) return cb(err)
      redis.HSET(redisId, 'timestamp', Date.now(), (err, res) => {
        if (err) return logger(err)
        jwt.sign(redisId, process.env.JWT_SECRET, cb)
      })
    }
  )
}

// MODIF
function updateAuth(redisId, update, cb) {
  every(
    Object.keys(update).map(key => {
      const pair = {}
      pair[key] = update[key]
      return pair
    }),
    (pair, callback) => {
      const key = Object.keys(pair)
      if (key.length > 1)
        return callback(
          new Error(
            '500: Error during user object decomposition for Redis newAuth()'
          )
        )
      redis.HSET(redisId, key[0], update[key[0]], callback)
    },
    (err, res) => {
      if (err) return cb(err)
      cb(null, redisId)
    }
  )
}

//SUPPRESSION
function delAuth(token, cb) {
  tokenExtraction(extractBearerToken(token), (err, redisId) => {
    if (err) return cb(err)
    redis.DEL(redisId, cb)
  })
}

module.exports = {
  extractBearerToken,
  tokenExtraction,
  newAuth,
  updateAuth,
  delAuth,
}
