const { appendFile } = require('fs')

// To construct logger and technical errors
function getDateTime() {
  let date = new Date()
  let hour = date.getHours()
  hour = (hour < 10 ? '0' : '') + hour
  let min = date.getMinutes()
  min = (min < 10 ? '0' : '') + min
  let sec = date.getSeconds()
  sec = (sec < 10 ? '0' : '') + sec
  let year = date.getFullYear()
  let month = date.getMonth() + 1
  month = (month < 10 ? '0' : '') + month
  let day = date.getDate()
  day = (day < 10 ? '0' : '') + day
  return day + '/' + month + '/' + year + ' - ' + hour + ':' + min + ':' + sec
}

// Logger to technicals errors
function logger(err) {
  if (err.type !== 'DisplayError') {
    const now = getDateTime()
    const error = now + ' - ' + err + '\n'
    appendFile('./logs/errors.log', error, fileError => {
      {
        fileError && console.log(fileError)
        console.log(error)
      }
    })
    return '500: Oups something wrong at ' + now + ', contact your admin'
  }
  return err.message
}

//The Error of type DiplayError
function DisplayError(message) {
  this.name = 'DisplayError'
  this.type = 'DisplayError'
  this.message = message || '418: Oups, water too cold'
}
DisplayError.prototype = Error.prototype

// To send success response
function success(res, data) {
  const response = {
    error: null,
    data: data.rows ? data.rows : data,
  }
  res.status(200).json(response)
}

// To send error response
//@todo : gestion status
function error(res, err) {
  err = logger(err)
  err = err.message ? err.message : err
  let code = parseInt(err.split(': ')[0])
  if (isNaN(code)) code = 500
  const response = {
    error: err,
    data: null,
  }
  res.status(code).json(response)
}

module.exports = {
  success,
  error,
  DisplayError,
  logger,
}
