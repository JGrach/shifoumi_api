const jwt = require('jsonwebtoken'),
  { waterfall } = require('async')
const send = require('./helpers'),
  { DisplayError } = send,
  redis = require('../redis'),
  auth = require('./auth'),
  env = process.env.NODE_ENV || 'development',
  {
    api: { version },
  } = require('../config.json')[env]

// logger
function logger(req, res, next) {
  console.log('request => ' + '\x1b[34m' + req.originalUrl + '\x1b[0m')
  next()
}

// AUTHENTICATION MIDDELWARE
function connected(req, res, next) {
  waterfall(
    [
      callback => {
        const token = req.headers.authorization
        if (!token) return next()
        callback(null, token)
      },
      (token, callback) => {
        auth.tokenExtraction(auth.extractBearerToken(token), callback)
      },
      (redisId, callback) => {
        redis.HGETALL(redisId, (err, res) => {
          if (err) return callback(err)
          if (!res)
            return callback(
              new DisplayError(
                '401: Token invalid or expired, please reconnect'
              )
            )
          callback(null, redisId, res)
        })
      },
      (redisId, user, callback) => {
        auth.updateAuth(redisId, { timestamp: Date.now() }, (err, res) => {
          if (err) return callback(err)
          callback(null, user)
        })
      },
      (user, callback) => {
        const request = req.url.split('/')
        const base = request[2]
        const id = request[3]
        user.isAdmin = user.isAdmin == 'true'
        user.isMe = (base == 'user' || base == 'users') && id && id == user.id
        callback(null, user)
      },
    ],
    (err, user) => {
      if (err) return send.error(res, err)
      req.userInformations = user
      next()
    }
  )
}

function isConnected(req, res, next) {
  if (!req.userInformations)
    return send.error(res, new DisplayError('401: You have to be connected !'))
  next()
}

function isAdmin(req, res, next) {
  req.userInformations && req.userInformations.isAdmin
    ? next()
    : send.error(res, new DisplayError('403: You have to be admin'))
}

function isMe(req, res, next) {
  req.userInformations && req.userInformations.isAdmin
    ? next()
    : req.userInformations && req.userInformations.isMe
      ? next()
      : send.error(res, new DisplayError("403: You're not the owner"))
}

module.exports = {
  logger,
  connected,
  isConnected,
  isAdmin,
  isMe,
}
