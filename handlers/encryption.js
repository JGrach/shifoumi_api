const bcrypt = require('bcrypt-nodejs')
const { compare } = bcrypt

function encode(pass, cb) {
  bcrypt.genSalt(10, (err, salt) => {
    if (err) return cb(err)
    bcrypt.hash(pass, salt, null, cb)
  })
}

module.exports = {
  encode,
  compare,
}
