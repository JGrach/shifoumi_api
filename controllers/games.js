const { validate } = require('joi'),
  { waterfall, map } = require('async')
const Joi = require('../handlers/schema'),
  { gameSchema, gameSchemaAll } = Joi,
  send = require('../handlers/helpers'),
  { DisplayError } = send,
  games_queries = require('../model/games'),
  rules_queries = require('../model/rules'),
  users_queries = require('../model/users'),
  warriors_queries = require('../model/warriors')

const getGames = (req, res) => {
  waterfall(
    [
      callback => {
        games_queries.getAll(callback)
      },
    ],
    (err, result) => {
      err ? send.error(res, err) : send.success(res, result)
    }
  )
}

const getGameBy = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax
        const data = req.query
        const schema = gameSchemaAll.forbiddenKeys('users_objects')
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        if (!data.users_ids) return callback(null, data)
        // Verification semantic users_ids are not same
        const users_ids = data.users_ids
          .slice()
          .sort((users_id, users_id_next) => {
            return users_id - users_id_next
          })
        const double = users_ids.reduce((trigger, users_id, index) => {
          return trigger
            ? trigger
            : users_id === users_ids[index + 1]
        }, false)
        double
          ? callback(new DisplayError('two or more users_ids are the same'))
          : callback(null, data)
      },
      (data, callback) => {
        if (!data.warriors_ids) return callback(null, data)
        // Verification semantic warriors_ids are not same
        const warriors_ids = data.warriors_ids.slice().sort()
        const double = warriors_ids.reduce((trigger, warriors_id, index) => {
          return trigger ? trigger : warriors_id === warriors_ids[index + 1]
        }, false)
        double
          ? callback(new DisplayError('two or more warriors_ids are the same'))
          : callback(null, data)
      },
      (data, callback) => {
        if (!data.rules_id) return callback(null, data, false)
        // Verification semantic rule
        rules_queries.getBy({ id: data.rules_id }, (err, searchRule) => {
          err ? callback(err) : callback(null, data, searchRule)
        })
      },
      (game, searchRule, callback) => {
        if (!searchRule) return callback(null, game)
        // Reject doesn't exist
        searchRule.length < 1
          ? callback(new DisplayError("400: this rules_id doesn't exist"))
          : callback(null, game)
      },
      (game, callback) => {
        if (!game.users_ids) return callback(null, game)
        // Verification semantic user
        map(
          game.users_ids,
          (users_id, cb) => {
            users_queries.getBy({ id: users_id }, cb)
          },
          (err, results) => {
            if (err) return callback(err)
            const noResults = results.reduce((trigger, result) => {
              return trigger ? trigger : result.length < 1
            }, false)
            noResults
              ? callback(new DisplayError("one of this user doesn't exist"))
              : callback(null, game)
          }
        )
      },
      (game, callback) => {
        if (!game.warriors_ids) return callback(null, game)
        // Verification semantic warrior
        map(
          game.warriors_ids,
          (warriors_id, cb) => {
            warriors_queries.getBy({ id: warriors_id }, cb)
          },
          (err, results) => {
            if (err) return callback(err)
            const noResults = results.reduce((trigger, result) => {
              return trigger ? trigger : result.length < 1
            }, false)
            noResults
              ? callback(new DisplayError("one of this warrior doesn't exist"))
              : callback(null, game)
          }
        )
      },
      (game, callback) => {
        if (game.id) {
          game['games.id'] = game.id
          delete game.id
        }
        // query
        games_queries.getBy(game, callback)
      },
    ],
    (err, result) => {
      err ? send.error(res, err) : send.success(res, result)
    }
  )
}

const addGame = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax
        const data = req.body
        let schema = gameSchema.requiredKeys(
          'rules_id',
          'users_objects',
          'warriors_ids'
        )
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        // Verification semantic users_ids are not same
        const users_objects = data.users_objects
          .slice()
          .sort((user_object, user_object_next) => {
            return user_object.id - user_object_next.id
          })
        const double = users_objects.reduce((trigger, user_object, index) => {
          return trigger
            ? trigger
            : user_object.id === users_objects[index + 1] && users_objects[index + 1].id
        }, false)
        double
          ? callback(
              new DisplayError('400: two or more users_objects are the same')
            )
          : callback(null, data)
      },
      (data, callback) => {
        // Verification semantic warriors_ids are not same
        const warriors_ids = data.warriors_ids.slice().sort()
        const double = warriors_ids.reduce((trigger, warriors_id, index) => {
          return trigger ? trigger : warriors_id === warriors_ids[index + 1]
        }, false)
        double
          ? callback(
              new DisplayError('400: two or more warriors_ids are the same')
            )
          : callback(null, data)
      },
      (data, callback) => {
        // Verification semantic rule
        rules_queries.getBy({ id: data.rules_id }, (err, searchRule) => {
          err ? callback(err) : callback(null, data, searchRule)
        })
      },
      (game, searchRule, callback) => {
        // Reject doesn't exist
        searchRule.length < 1
          ? callback(new DisplayError("400: this rules_id doesn't exist"))
          : callback(null, game)
      },
      (game, callback) => {
        // Verification semantic user
        map(
          game.users_objects,
          (users_id, cb) => {
            users_queries.getBy({ id: users_id.id }, cb)
          },
          (err, results) => {
            if (err) return callback(err)
            const noResults = results.reduce((trigger, result) => {
              return trigger ? trigger : result.length < 1
            }, false)
            noResults
              ? callback(
                  new DisplayError("400: one of this user doesn't exist")
                )
              : callback(null, game)
          }
        )
      },
      (game, callback) => {
        // Verification semantic warrior
        map(
          game.warriors_ids,
          (warriors_id, cb) => {
            warriors_queries.getBy({ id: warriors_id }, cb)
          },
          (err, results) => {
            if (err) return callback(err)
            const noResults = results.reduce((trigger, result) => {
              return trigger ? trigger : result.length < 1
            }, false)
            noResults
              ? callback(
                  new DisplayError("400: one of this warrior doesn't exist")
                )
              : callback(null, game)
          }
        )
      },
      (game, callback) => {
        // query
        games_queries.add({ rules_id: game.rules_id }, (err, added) => {
          err ? callback(err) : callback(null, added[0], game)
        })
      },
      (id, game, callback) => {
        // query
        map(
          game.users_objects,
          (users_id, cb) => {
            games_queries.addRelation(
              'users',
              {
                users_id: users_id.id,
                games_id: id,
                isWinner: users_id.isWinner,
              },
              (err, added) => {
                err ? cb(err) : cb(null, added)
              }
            )
          },
          (err, results) => {
            err ? callback(err) : callback(null, id, game)
          }
        )
      },
      (id, game, callback) => {
        // query
        map(
          game.warriors_ids,
          (warriors_id, cb) => {
            games_queries.addRelation(
              'warriors',
              { warriors_id: warriors_id, games_id: id },
              (err, added) => {
                err ? cb(err) : cb(null, added)
              }
            )
          },
          (err, results) => {
            err ? callback(err) : callback(null, id, game)
          }
        )
      },
    ],
    (err, query, game) => {
      err
        ? send.error(res, err)
        : send.success(res, 'added game with id: ' + query)
    }
  )
}

const updateGame = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax params
        const data = req.params
        const schema = Joi.id
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (id, callback) => {
        //Verification syntax body
        const data = req.body
        const schema = gameSchema
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, { ...id, ...data })
      },
      (data, callback) => {
        // Verification semantic users_ids are not same
        const users_ids = data.users_ids
          .slice()
          .sort((user_object, user_object_next) => {
            return user_object.id - user_object_next.id
          })
        const double = users_ids.reduce((trigger, user_object, index) => {
          return trigger
            ? trigger
            : user_object.id === users_ids[index + 1] && users_ids[index + 1].id
        }, false)
        double
          ? callback(
              new DisplayError('400: two or more users_ids are the same')
            )
          : callback(null, data)
      },
      (data, callback) => {
        // Verification semantic warriors_ids are not same
        const warriors_ids = data.warriors_ids.slice().sort()
        const double = warriors_ids.reduce((trigger, warriors_id, index) => {
          return trigger ? trigger : warriors_id === warriors_ids[index + 1]
        }, false)
        double
          ? callback(
              new DisplayError('400: two or more warriors_ids are the same')
            )
          : callback(null, data)
      },
      (data, callback) => {
        // Verification semantic rule
        rules_queries.getBy({ id: data.rules_id }, (err, searchRule) => {
          err ? callback(err) : callback(null, data, searchRule)
        })
      },
      (game, searchRule, callback) => {
        // Reject doesn't exist
        searchRule.length < 1
          ? callback(new DisplayError("400: this rules_id doesn't exist"))
          : callback(null, game)
      },
      (game, callback) => {
        // Verification semantic user
        map(
          game.users_ids,
          (users_id, cb) => {
            users_queries.getBy({ id: users_id.id }, cb)
          },
          (err, results) => {
            if (err) return callback(err)
            const noResults = results.reduce((trigger, result) => {
              return trigger ? trigger : result.length < 1
            }, false)
            noResults
              ? callback(
                  new DisplayError("400: one of this user doesn't exist")
                )
              : callback(null, game)
          }
        )
      },
      (game, callback) => {
        // Verification semantic warrior
        map(
          game.warriors_ids,
          (warriors_id, cb) => {
            warriors_queries.getBy({ id: warriors_id }, cb)
          },
          (err, results) => {
            if (err) return callback(err)
            const noResults = results.reduce((trigger, result) => {
              return trigger ? trigger : result.length < 1
            }, false)
            noResults
              ? callback(
                  new DisplayError("400: one of this warrior doesn't exist")
                )
              : callback(null, game)
          }
        )
      },
      (game, callback) => {
        // query
        games_queries.add({ rules_id: game.rules_id }, (err, added) => {
          err ? callback(err) : callback(null, added[0], game)
        })
      },
      (id, game, callback) => {
        // query
        map(
          game.users_ids,
          (users_id, cb) => {
            games_queries.addRelation(
              'users',
              {
                users_id: users_id.id,
                games_id: id,
                isWinner: users_id.isWinner,
              },
              (err, added) => {
                err ? cb(err) : cb(null, added)
              }
            )
          },
          (err, results) => {
            err ? callback(err) : callback(null, id, game)
          }
        )
      },
      (id, game, callback) => {
        // query
        map(
          game.warriors_ids,
          (warriors_id, cb) => {
            games_queries.addRelation(
              'warriors',
              { warriors_id: warriors_id, games_id: id },
              (err, added) => {
                err ? cb(err) : cb(null, added)
              }
            )
          },
          (err, results) => {
            err ? callback(err) : callback(null, id, game)
          }
        )
      },
    ],
    (err, game) => {
      err
        ? send.error(res, err)
        : send.success(res, 'updated game with id: ' + game.id)
    }
  )
}

const deleteGame = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax
        const data = req.params
        const schema = Joi.id
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        // Verification semantic game
        games_queries.getBy({ id: data.id }, (err, searchGame) => {
          err ? callback(err) : callback(null, data, searchGame)
        })
      },
      (game, searchGame, callback) => {
        // Reject if doesn't exist
        searchGame.length < 1
          ? callback(new DisplayError('400: game not found'))
          : callback(null, game)
      },
      (data, callback) => {
        // query
        games_queries.delGame(
          data,
          (err, resolve) =>
            err ? callback(err) : callback(null, data, resolve)
        )
      },
    ],
    (err, game, result) => {
      err
        ? send.error(res, err)
        : send.success(
            res,
            'Game with id: ' + game.id + ' has been successfully deleted'
          )
    }
  )
}

module.exports = {
  getGames,
  getGameBy,
  addGame,
  updateGame,
  deleteGame,
}
