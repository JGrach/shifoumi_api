const { validate } = require('joi'),
  { waterfall } = require('async')
const Joi = require('../handlers/schema'),
  { measureSchemaAll } = Joi,
  send = require('../handlers/helpers'),
  { DisplayError } = send,
  measures_queries = require('../model/measures')
  
const roundsQuery = (data, cb) => {
  if(Object.keys(data).length === 0){
    measures_queries.getRoundResultBy(data, cb)
  } else {
    measures_queries.getRoundResultAll(cb)
  }
}

const quantity = (data, callback) => {
  roundsQuery(data, (err, res) => {
    if(err) return callback(err)
    const countByWeapon = res.reduce((countByWeapon, warrior, i) => {
      if(!countByWeapon[warrior.weapon]) countByWeapon[warrior.weapon] = { win: 0, loose: 0, equal: 0, all: 0 }
      countByWeapon[warrior.weapon].all ++
      if(warrior.isWinner){
        countByWeapon[warrior.weapon].win ++
      } else {
        const adversary = res.find((warriorAdversary, index) => 
          warriorAdversary.games_id === warrior.games_id &&
          warriorAdversary.order === warrior.order &&
          i != index
        )
        if(adversary.isWinner){
          countByWeapon[warrior.weapon].loose ++
        } else {
          countByWeapon[warrior.weapon].equal ++
        }
      }
      return countByWeapon
    }, {})
    return callback(null, countByWeapon)
  })
}

const getMeasures = (req, res) => {
  waterfall([
    callback => {
      //Verification syntax
      const data = {...req.query, ...req.params}
      const schema = measureSchemaAll
      const notValid = !data || validate(data, schema).error
      notValid
        ? callback(new DisplayError('400: params have an inexpected format'))
        : callback(null, data)
    },
    (data, callback) => {
      const name = data.name
      delete data.name
      if(data.pov === 'mine'){
        data['users.id'] = req.userInformations.id
        delete data.pov
      }
      // Query
      switch(name){
        case 'quantity': 
        default:
          quantity(data, callback)
        break
      }
    }
  ], (err, result) => {
    err ? send.error(res, err) : send.success(res, result)
  })
}

module.exports = {
  getMeasures
}