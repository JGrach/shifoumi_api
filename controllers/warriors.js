const { validate } = require('joi'),
  { waterfall } = require('async')
const Joi = require('../handlers/schema'),
  { warriorSchema, warriorSchemaAll } = Joi,
  send = require('../handlers/helpers'),
  { DisplayError } = send,
  warriors_queries = require('../model/warriors'),
  users_queries = require('../model/users')

const getWarriors = (req, res) => {
  waterfall(
    [
      callback => {
        warriors_queries.getAll(callback)
      },
    ],
    (err, result) => {
      err ? send.error(res, err) : send.success(res, result)
    }
  )
}

const getWarriorBy = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax
        const data = req.query
        const schema = warriorSchemaAll
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        //Verification semantic
        if (!data.users_id) return callback(null, data, false)
        users_queries.getBy({ id: data.users_id }, (err, searchUser) => {
          err ? callback(err) : callback(null, data, searchUser)
        })
      },
      (data, searchUser, callback) => {
        // Reject if doesn't exist
        if (!searchUser) return callback(null, data)
        searchUser.length < 1
          ? callback(new DisplayError("400: this users_id doesn't exist"))
          : callback(null, data)
      },
      (data, callback) => {
        // query
        warriors_queries.getBy(data, callback)
      },
    ],
    (err, result) => {
      err ? send.error(res, err) : send.success(res, result)
    }
  )
}

const addWarrior = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax
        const data = req.body
        let schema = warriorSchema.requiredKeys('isWinner', 'order', 'users_id')
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        // Verification semantic
        users_queries.getBy({ id: data.users_id }, (err, searchUser) => {
          err ? callback(err) : callback(null, data, searchUser)
        })
      },
      (warrior, searchUser, callback) => {
        // Reject doesn't exist
        searchUser.length < 1
          ? callback(new DisplayError("400: this user_id doesn't exist"))
          : callback(null, warrior)
      },
      (warrior, callback) => {
        // query
        warriors_queries.add(warrior, (err, added) => {
          err ? callback(err) : callback(null, added, warrior)
        })
      },
    ],
    (err, query, warrior) => {
      err
        ? send.error(res, err)
        : send.success(res, 'added warrior with id: ' + query)
    }
  )
}

const updateWarrior = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax params
        const data = req.params
        const schema = Joi.id
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (id, callback) => {
        //Verification syntax body
        const data = req.body
        const schema = warriorSchema
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, { ...id, ...data })
      },
      (data, callback) => {
        // Verification semantic user
        if (!data.users_id) return callback(null, data, false)
        users_queries.getBy({ id: data.users_id }, (err, searchUser) => {
          err ? callback(err) : callback(null, data, searchUser)
        })
      },
      (data, searchUser, callback) => {
        // Reject if doesn't exist
        if (!searchUser) return callback(null, data)
        searchUser.length < 1
          ? callback(new DisplayError("400: this users_id doesn't exist"))
          : callback(null, data)
      },
      (data, callback) => {
        // Verification semantic warrior
        warriors_queries.getBy({ id: data.id }, (err, searchWarrior) => {
          err ? callback(err) : callback(null, data, searchWarrior)
        })
      },
      (warrior, searchWarrior, callback) => {
        // Reject if doesn't exist
        searchWarrior.length < 1
          ? callback(new DisplayError('400: warrior not found'))
          : callback(null, warrior)
      },
      (warrior, callback) => {
        // query
        warriors_queries.update({ id: warrior.id }, warrior, (err, updated) => {
          err ? callback(err) : callback(null, warrior)
        })
      },
    ],
    (err, warrior) => {
      err
        ? send.error(res, err)
        : send.success(res, 'updated warrior with id: ' + warrior.id)
    }
  )
}

const deleteWarrior = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax
        const data = req.params
        const schema = Joi.id
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        // Verification semantic warrior
        warriors_queries.getBy({ id: data.id }, (err, searchWarrior) => {
          err ? callback(err) : callback(null, data, searchWarrior)
        })
      },
      (warrior, searchWarrior, callback) => {
        // Reject if doesn't exist
        searchWarrior.length < 1
          ? callback(new DisplayError('400: warrior not found'))
          : callback(null, warrior)
      },
      (data, callback) => {
        // query
        warriors_queries.delWarrior(
          data,
          (err, resolve) =>
            err ? callback(err) : callback(null, data, resolve)
        )
      },
    ],
    (err, warrior, result) => {
      err
        ? send.error(res, err)
        : send.success(
            res,
            'Warrior with id: ' + warrior.id + ' has been successfully deleted'
          )
    }
  )
}

module.exports = {
  getWarriors,
  getWarriorBy,
  addWarrior,
  updateWarrior,
  deleteWarrior,
}
