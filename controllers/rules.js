const { validate } = require('joi'),
  { waterfall } = require('async')
const Joi = require('../handlers/schema'),
  { ruleSchema, ruleSchemaAll } = Joi,
  send = require('../handlers/helpers'),
  { DisplayError } = send,
  rules_queries = require('../model/rules')

const getRules = (req, res) => {
  waterfall(
    [
      callback => {
        rules_queries.getAll(callback)
      },
    ],
    (err, result) => {
      err ? send.error(res, err) : send.success(res, result)
    }
  )
}

const getRuleBy = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax
        const data = req.query
        const schema = ruleSchemaAll
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        // query
        rules_queries.getBy(data, callback)
      },
    ],
    (err, result) => {
      err ? send.error(res, err) : send.success(res, result)
    }
  )
}

const addRule = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax
        const data = req.body
        let schema = ruleSchema.requiredKeys(
          'title' /*, 'gameTime', 'roundTime'*/,
          'roundNumber'
        )
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        // Verification semantic
        rules_queries.getBy({ title: data.title }, (err, searchRule) => {
          err ? callback(err) : callback(null, data, searchRule)
        })
      },
      (rule, searchRule, callback) => {
        // Reject no unique
        searchRule.length > 0
          ? callback(new DisplayError('409: this rule title already exist'))
          : callback(null, rule)
      },
      (rule, callback) => {
        // query
        rules_queries.add(rule, (err, added) => {
          err ? callback(err) : callback(null, added, rule)
        })
      },
    ],
    (err, query, rule) => {
      err ? send.error(res, err) : send.success(res, 'added rule ' + rule.title)
    }
  )
}

const updateRule = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax params
        const data = req.params
        const schema = Joi.id
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (id, callback) => {
        //Verification syntax body
        const data = req.body
        const schema = ruleSchema
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, { ...id, ...data })
      },
      (data, callback) => {
        // Verification semantic
        rules_queries.getBy({ id: data.id }, (err, searchRule) => {
          err ? callback(err) : callback(null, data, searchRule)
        })
      },
      (rule, searchRule, callback) => {
        // Reject if doesn't exist
        searchRule.length !== 1
          ? callback(new DisplayError('400: rule not found'))
          : callback(null, rule)
      },
      (rule, callback) => {
        // If title, test it
        if (!rule.title) return callback(null, rule, false)
        rules_queries.getBy({ title: rule.title }, (err, searchRule) => {
          err ? callback(err) : callback(null, rule, searchRule)
        })
      },
      (rule, searchRule, callback) => {
        // Reject if already exist
        if (searchRule) {
          if (searchRule.length > 0 && searchRule[0].id != req.params.id)
            return callback(
              new DisplayError('409: this rule title already exist')
            )
        }
        callback(null, rule)
      },
      (rule, callback) => {
        // query
        rules_queries.update({ id: rule.id }, rule, (err, updated) => {
          err ? callback(err) : callback(null, rule)
        })
      },
    ],
    (err, rule) => {
      err
        ? send.error(res, err)
        : send.success(res, 'updated rule ' + rule.title)
    }
  )
}

const deleteRule = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax
        const data = req.params
        const schema = Joi.id
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        // Verification semantic
        rules_queries.getBy({ id: data.id }, (err, searchRule) => {
          err ? callback(err) : callback(null, data, searchRule)
        })
      },
      (rule, searchRule, callback) => {
        // Reject if doesn't exist
        searchRule.length !== 1
          ? callback(new DisplayError('400: rule not found'))
          : callback(null, rule)
      },
      (data, callback) => {
        // query
        rules_queries.delRule(
          data,
          (err, resolve) =>
            err ? callback(err) : callback(null, data, resolve)
        )
      },
    ],
    (err, rule, result) => {
      err
        ? send.error(res, err)
        : send.success(
            res,
            'Rule with id: ' + rule.id + ' has been successfully deleted'
          )
    }
  )
}

module.exports = {
  getRules,
  getRuleBy,
  addRule,
  updateRule,
  deleteRule,
}
