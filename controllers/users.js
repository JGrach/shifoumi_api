const joi = require('joi'),
  { validate } = joi,
  { waterfall } = require('async'),
  { encode } = require('../handlers/encryption')
const Joi = require('../handlers/schema'),
  { userSchema, userSchemaAll } = Joi,
  send = require('../handlers/helpers'),
  { DisplayError } = send,
  { newAuth, updateAuth, delAuth } = require('../handlers/auth'),
  users_queries = require('../model/users')

const getUsers = (req, res) => {
  const admin = req.userInformations && req.userInformations.isAdmin
  waterfall(
    [
      callback => {
        // query
        users_queries.getAll(callback, admin, false)
      },
      (result, callback) => {
        if (admin) return callback(null, result)
        // Verification outputs
        const schema = Joi.arrayOf(
          userSchemaAll.forbiddenKeys('password', 'mailAddress')
        )
        joi.assert([{ isAdmin: false }], schema)
        const notValid = validate(result, schema).error
        notValid ? callback(notValid) : callback(null, result)
      },
    ],
    (err, result) => {
      err ? send.error(res, err) : send.success(res, result)
    }
  )
}

const getUser = (req, res) => {
  const admin = req.userInformations && req.userInformations.isAdmin
  const mine = req.userInformations && req.userInformations.isMe
  waterfall(
    [
      callback => {
        //Verification inputs
        const data = req.params
        const schema = Joi.id
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        // query
        users_queries.getBy(data, callback, admin || mine, false)
      },
      (result, callback) => {
        if (admin || mine) return callback(null, result)
        // Verification outputs
        const schema = Joi.arrayOf(
          userSchemaAll.forbiddenKeys('password', 'mailAddress'),
          1
        )
        joi.assert([{ isAdmin: false }], schema)
        const notValid = validate(result, schema).error
        notValid ? callback(notValid) : callback(null, result)
      },
    ],
    (err, result) => {
      err ? send.error(res, err) : send.success(res, result)
    }
  )
}

const updateUser = (req, res) => {
  const admin = req.userInformations && req.userInformations.isAdmin
  const mine = req.userInformations && req.userInformations.isMe
  waterfall(
    [
      callback => {
        //Verification inputs
        const data = req.body
        let schema = userSchema
        if (!admin) schema.forbiddenKeys('isAdmin')
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        // Verification exist
        users_queries.getBy(
          { id: req.params.id },
          (err, searchUser) => {
            err ? callback(err) : callback(null, data, searchUser)
          },
          mine || admin
        )
      },
      (user, searchUser, callback) => {
        // Reject if doesn't exist
        searchUser.length !== 1
          ? callback(new DisplayError('400: user not found'))
          : callback(null, user)
      },
      (user, callback) => {
        // If mailAdress, test it
        if (!user.mailAdress) return callback(null, user, false)
        users_queries.getBy(
          { mailAdress: user.mailAdress },
          (err, searchUser) => {
            err ? callback(err) : callback(null, user, searchUser)
          }
        )
      },
      (user, searchUser, callback) => {
        // Reject if already exist
        if (searchUser) {
          if (
            (searchUser.length > 0 &&
              searchUser[0].id != req.userInformations.id) ||
            searchUser[0].id != req.params.id
          )
            return callback(
              new DisplayError('409: this email address already exist')
            )
        }
        callback(null, user)
      },
      (user, callback) => {
        // Encode Password if necessary
        user.password
          ? encode(user.password, (err, hash) => {
              if (err) return callback(err)
              user.password = hash
              callback(null, user)
            })
          : callback(null, user)
      },
      (user, callback) => {
        // query
        users_queries.update(
          { id: req.userInformations.id || (admin && req.params.id) },
          user,
          (err, updated) => {
            err ? callback(err) : callback(null, user)
          }
        )
      },
      (user, callback) => {
        if (admin) return callback(null, user)
        updateAuth(
          req.headers.authorization,
          { login: user.login, isAdmin: user.isAdmin || false },
          (err, res) => {
            if (err) return callback(err)
            return callback(null, user, res)
          }
        )
      },
    ],
    (err, user) => {
      if (err) return send.error(res, err)
      if (admin)
        return send.success(
          res,
          'updated user ' + user.login + ' have to be reconnect to have change'
        )
      const response = {
        status: 'updated user ' + user.login,
      }
      send.success(res, response)
    }
  )
}

const deleteUser = (req, res) => {
  const admin = req.userInformations && req.userInformations.isAdmin
  const mine = req.userInformations && req.userInformations.isMe
  waterfall(
    [
      callback => {
        //Verification inputs
        const data = req.params
        const schema = Joi.id
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        // Verification exist
        users_queries.getBy(
          { id: req.params.id },
          (err, searchUser) => {
            err ? callback(err) : callback(null, data, searchUser)
          },
          mine || admin
        )
      },
      (user, searchUser, callback) => {
        // Reject if doesn't exist
        searchUser.length !== 1
          ? callback(new DisplayError('400: user not found'))
          : callback(null, user)
      },
      (data, callback) => {
        // query
        users_queries.delUser(
          data,
          (err, resolve) =>
            err ? callback(err) : callback(null, data, resolve)
        )
      },
    ],
    (err, user, result) => {
      if (err) return send.error(res, err)
      if (admin)
        return send.success(
          res,
          'User with id: ' + user.id + ' has been successfully deleted'
        )
      send.success(res, 'User has been successfully deleted')
      delAuth(req.headers.authorization)
    }
  )
}

module.exports = {
  getUsers,
  getUser,
  updateUser,
  deleteUser,
}
