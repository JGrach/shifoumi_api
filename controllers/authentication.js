const { validate } = require('joi'),
  { waterfall } = require('async')
const Joi = require('../handlers/schema'),
  { userSchema, userSchemaAll } = Joi,
  send = require('../handlers/helpers'),
  { DisplayError } = send,
  { encode, compare } = require('../handlers/encryption'),
  users_queries = require('../model/users'),
  { newAuth, delAuth } = require('../handlers/auth')

const addUser = (req, res) => {
  const admin = req.userInformations && req.userInformations.isAdmin
  waterfall(
    [
      callback => {
        //Verification syntax
        const data = req.body
        let schema = userSchema.requiredKeys('login', 'mailAddress', 'password')
        if (!admin) schema.forbiddenKeys('isAdmin')
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        // Verification semantic
        users_queries.getBy(
          { mailAddress: data.mailAddress },
          (err, searchUser) => {
            err ? callback(err) : callback(null, data, searchUser)
          }
        )
      },
      (user, searchUser, callback) => {
        // Reject no unique
        searchUser.length > 0
          ? callback(new DisplayError('409: this email address already exist'))
          : callback(null, user)
      },
      (user, callback) => {
        // Encode password
        encode(user.password, (err, hash) => {
          if (err) return callback(err)
          user.password = hash
          callback(null, user)
        })
      },
      (user, callback) => {
        // query
        users_queries.add(user, (err, added) => {
          err ? callback(err) : callback(null, added, user)
        })
      },
      (query, user, callback) => {
        // token generation
        if (admin) return callback(null, user)
        newAuth(
          { id: query[0], login: user.login, isAdmin: false },
          (err, res) => {
            if (err) return callback(err)
            callback(null, user, res)
          }
        )
      },
    ],
    (err, user, token) => {
      if (err) return send.error(res, err)
      if (admin) return send.success(res, 'added user ' + user.login)
      const response = {
        status: 'added user ' + user.login + ' your token is join',
        token,
        user: {
          id: user.id,
          login: user.login
        }
      }
      send.success(res, response)
      req.headers.authorization && delAuth(req.headers.authorization)
    }
  )
}

const login = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax
        const data = req.body
        const schema = userSchema
          .requiredKeys('password')
          .forbiddenKeys('isAdmin')
        const notValid =
          !data ||
          !(data.login || data.mailAddress) ||
          data.length > 2 ||
          validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (data, callback) => {
        const constraint = data.login
          ? { login: data.login }
          : { mailAddress: data.mailAddress }
        //query
        users_queries.getBy(
          constraint,
          (err, result) => {
            if (err) return callback(err)
            result.length > 0
              ? callback(null, data, result[0])
              : callback(new DisplayError("400: user doesn't exist"))
          },
          true
        )
      },
      (data, result, callback) => {
        // Compare pass
        compare(data.password, result.password, (err, boolean) => {
          if (err) return callback(err)
          boolean
            ? callback(null, result)
            : callback(new DisplayError('400: password wrong'))
        })
      },
      (user, callback) => {
        // token generation
        newAuth(
          {
            id: user.id,
            login: user.login,
            isAdmin: user.isAdmin,
          },
          (err, res) => {
            if (err) return callback(err)
            callback(null, res, user)
          }
        )
      },
    ],
    (err, token, user) => {
      if (err) return send.error(res, err)
      const { id, login, mailAddress } = user
      const response = {
        status: 'welcome ' + user.login + ' your token is join',
        user: { id, login, mailAddress },
        token,
      }
      send.success(res, response)
      req.headers.authorization && delAuth(req.headers.authorization)
    }
  )
}

const logout = (req, res) => {
  waterfall(
    [
      callback => {
        //Verification syntax
        const data = req.params
        const schema = Joi.id
        const notValid = !data || validate(data, schema).error
        notValid
          ? callback(new DisplayError('400: params have an inexpected format'))
          : callback(null, data)
      },
      (user, callback) => {
        req.headers.authorization &&
          delAuth(req.headers.authorization, callback)
      },
    ],
    (err, result) => {
      if (err) return send.error(res, err)
      send.success(res, "You're deconnected")
    }
  )
}

module.exports = {
  addUser,
  login,
  logout,
}
