const knex = require('./model/knex.js')
const env = process.env.NODE_ENV || 'development'

if(env === 'development') require('dotenv').config()

function shouldMigrate(res){
  knex.migrate.currentVersion().then(res => {
    if(res !== "none") return process.exit(0)
    console.log("Database is empty, knex will migrate and seed")
    return knex.migrate.latest()
  }).then(res => {
    return knex.seed.run();
  }).then(res => {
    process.exit(0)
  })
  .catch( err => {
    console.log(err)
    process.exit(1);
  })
}

const run = process.env.KNEX_MIGRATE == 'true' ? knex.migrate.rollback().then( res => {
    console.log('Knex will rollback migration on the first run because process env asking')
    process.env.KNEX_MIGRATE == 'false'
    return res
  }) : Promise.resolve(false)
run.then( shouldMigrate )


