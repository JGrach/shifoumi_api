FROM node:latest

# Create app directory
WORKDIR /usr/src/shifoumi

ADD . .

RUN npm update && npm install
RUN npm install -global knex

EXPOSE 8080
ENTRYPOINT ["npm", "run"]
CMD [ "start" ]
